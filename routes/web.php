<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('service', function () {
//     return view('service.service1');
// });

// Route::post('/service', "ParcelController@getParcel")->name('parcel.details');


// Route::get('service2', function () {
//     return view('service.service2');
// });

// Route::get('service3', function () {
//     return view('service.service3');
// });
// Route::get('service4', function () {
//     return view('service.service4');
// });

//new Route
Route::get('/service', "ParcelController@getParcel");
Route::post('/service', "ParcelController@saveParcel")->name('parcel.details');

Route::get('/service2', "ParcelController@getdetails")->name('service2');
Route::post('/service2', "ParcelController@savedetails")->name('save.service2');

Route::get('/service3/{id}', "ParcelController@getcourier")->name('service3');

Route::get('/service4/{id}', "ParcelController@checkout")->name('checkout');
