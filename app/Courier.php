<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $table = 'courier';

    protected $fillable = ['parcel_content','value','weight','courier_type','delivery_days','price'];

    public function address()
    {
        return $this->hasOne('App\Address', 'parcels_id');
    }
}
