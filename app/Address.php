<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'parcelDetails';

    protected $fillable = ['parcels_id','name_sender','email_sender','contact_sender','alt_contact_sender','company_sender','address_sender','address_sender1','address_sender2','city_sender','postcode_sender','state_sender','country_sender','name_reciever','email_reciever','contact_reciever','alt_contact_reciever','company_reciever','address_reciever','address_reciever1','address_reciever2','city_reciever','postcode_reciever','state_reciever','country_reciever'];

     public function courier()
    {
        return $this->belongsTo('App\Courier');
    }
}
