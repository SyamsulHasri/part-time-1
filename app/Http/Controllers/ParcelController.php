<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Courier;
use App\Address;

class ParcelController extends Controller
{
    public function getParcel()
    {
        return view ('service.service1');
    }

    public function saveParcel(Request $request)
    {
        //FOR SAVE FILE/IMAGE
        // if($request->hasfile('filename'))
        //  {
        //     $file = $request->file('filename');
        //     $name=time().$file->getClientOriginalName();
        //     $file->move(public_path().'/images/', $name);
        //  }

        $courier= new Courier;
        $courier->parcel_content=$request->get('parcel_content');
        $courier->value=$request->get('value');
        $courier->weight=$request->get('weight');
        $courier->courier_type=$request->get('courier_type');
        $courier->delivery_days=$request->get('delivery_days');
        $courier->price=$request->get('price');
        $courier->save();

        $id = $courier->id;

        return redirect()->route('service2', ['id' => $id]);
    }

    public function getdetails()
    {
        return view ('service.service2');
    }

    public function savedetails(Request $request)
    {
        Address::create($request->all());

        $id = $request->get('parcels_id');


        return redirect()->route('service3', ['id' => $id]);
    }

    public function getcourier($id)
    {
        $courier_detail = Courier::find($id);
        // dd($courier_detail);

        return view ('service.service3', compact('courier_detail'));
    }

        public function checkout($id)
    {
        $courier_detail = Courier::find($id);

        return view ('service.service4', compact('courier_detail'));
    }


}
