@extends('layout.default')

@section('content')

    <div class="container my-3 py-4">
        <div class="row bs-wizard" style="border-bottom:0;">

                <div class="col-lg-3 ">
                  <div class="text-center ">Parcel Details</div>
                  <div class="progress">
                      <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Delivery Details</div>
                  <div class="progress">
                      <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Order Summary</div>
                  <div class="progress">
                      <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3">
                  <div class="text-center">Payment</div>
                  <div class="progress">
                      <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>
        </div>
    </div>

      <div class="container">

            <form method="post" action="{{ route('save.service2')}}">
            {{ csrf_field() }}

                <input type="text" class="form-control col-9" value="{{Request::get('id')}}" name="parcels_id" hidden>
                <div class="row ml-4 mt-0">
                  <div class="col-5">
                    <h4 class="text-center mb-4 border-bottom">Your Details</h4>
                        <div class="form-group row">
                          <label class="col-3 float-right">Name</label>
                          <input type="text" class="form-control col-9" name="name_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Email</label>
                          <input type="text" class="form-control col-9" name="email_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Phone No.</label>
                          <input type="text" class="form-control col-9" name="contact_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Alt Phone No.</label>
                          <input type="text" class="form-control col-9" name="alt_contact_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Company</label>
                          <input type="text" class="form-control col-9" name="company_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Address</label>
                          <input type="text" class="form-control col-9" name="address_sender" >
                        </div>
                        <div class="form-group row">
                          <!-- <label class="col-3">Name</label> -->
                          <input type="text" class="form-control offset-3 col-9" name="address_sender1" >
                        </div>
                        <div class="form-group row">
                          <!-- <label class="col-3">Name</label> -->
                          <input type="text" class="form-control offset-3 col-9" name="address_sender2" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">City</label>
                          <input type="text" class="form-control col-9" name="city_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Postcode</label>
                          <input type="text" class="form-control col-9" name="postcode_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">State</label>
                          <input type="text" class="form-control col-9" name="state_sender" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Country</label>
                          <input type="text" class="form-control col-9" name="country_sender" >
                        </div>
                  </div>

                  <div class="col-2"></div>

                  <div class="col-5">
                    <h4 class="text-center mb-4 border-bottom">Reciever Details</h4>
                        <div class="form-group row">
                          <label class="col-3">Name</label>
                          <input type="text" class="form-control col-9" name="name_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Email</label>
                          <input type="text" class="form-control col-9" name="email_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Phone No.</label>
                          <input type="text" class="form-control col-9" name="contact_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Alt Phone No.</label>
                          <input type="text" class="form-control col-9" name="alt_contact_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Company</label>
                          <input type="text" class="form-control col-9" name="company_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Address</label>
                          <input type="text" class="form-control col-9" name="address_reciever" >
                        </div>
                        <div class="form-group row">
                          <!-- <label class="col-3">Name</label> -->
                          <input type="text" class="form-control offset-3 col-9" name="address_reciever1" >
                        </div>
                        <div class="form-group row">
                          <!-- <label class="col-3">Name</label> -->
                          <input type="text" class="form-control offset-3 col-9" name="address_reciever2" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">City</label>
                          <input type="text" class="form-control col-9" name="city_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Postcode</label>
                          <input type="text" class="form-control col-9" name="postcode_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">State</label>
                          <input type="text" class="form-control col-9" name="state_reciever" >
                        </div>
                        <div class="form-group row">
                          <label class="col-3">Country</label>
                          <input type="text" class="form-control col-9" name="country_reciever" >
                        </div>
                  </div>
                </div>

                  <div class="text-center pt-3 mb-5">
                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
                  </div>

            </form>
      </div>

@endsection
