@extends('layout.default')

@section('content')

    <div class="container my-5 py-5">
        <div class="row bs-wizard" style="border-bottom:0;">

                <div class="col-lg-3 ">
                  <div class="text-center ">Parcel Details</div>
                  <div class="progress bg-success">
                      <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Delivery Details</div>
                  <div class="progress bg-success">
                      <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Order Summary</div>
                  <div class="progress bg-success">
                      <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3">
                  <div class="text-center">Payment</div>
                  <div class="progress bg-warning">
                      <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>
        </div>
    </div>

      <div class="container mb-5 pb-5">
        <div class="row">
          <div class="col-4">
              <div class="card">
                <div class="card-header">
                  Your Order
                </div>
                <div class="card-body">
                  <div class="row border-bottom">
                    <h6 class="text-muted col-6">Delivery Charge</h6>
                    <p class="text-muted col-6"> RM {{$courier_detail->price}}</p>
                  </div>
                  <div class="row border-bottom">
                    <h6 class="col-6">Total</h6>
                    <p class="col-6"> RM {{$courier_detail->price}}</p>
                  </div>
                    <div class="mt-3">
                      <a href="#" class="btn btn-primary">Proceed Payment</a>
                    </div>
                </div>
              </div>
          </div>

          <div class="col-8">
            <div class="card">
              <div class="card-header">
                Summary
              </div>
              <div class="card-body">
                  <table class="table table-borderless">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Courier</th>
                        <th scope="col">Parcel</th>
                        <th scope="col">Sender</th>
                        <th scope="col">Reciever</th>
                        <th scope="col">Price</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>{{$courier_detail->courier_type}}</td>
                        <td>{{$courier_detail->parcel_content}}</td>
                        <td>{{$courier_detail->address->name_sender}}</td>
                        <td>{{$courier_detail->address->name_reciever}}</td>
                        <td>RM {{$courier_detail->price}}</td>
                      </tr>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>

        </div>


      </div>

@endsection
