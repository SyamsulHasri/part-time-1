@extends('layout.default')

@section('content')

    <div class="container my-5 py-5">
        <div class="row bs-wizard" style="border-bottom:0;">

                <div class="col-lg-3 ">
                  <div class="text-center ">Parcel Details</div>
                  <div class="progress">
                      <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Delivery Details</div>
                  <div class="progress">
                      <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Order Summary</div>
                  <div class="progress">
                      <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3">
                  <div class="text-center">Payment</div>
                  <div class="progress">
                      <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>
        </div>
    </div>

      <div class="container mb-5 pb-5">

          <div class="card">
            <div class="card-header">
              MY Cart
            </div>
            <div class="card-body">
              <div class="row">
                <div class="offset-1 col-3"> <p> <b>Parcel Content:</b> {{$courier_detail->parcel_content}}</p> </div>
                <div class="col-3"> <p> <b>Parcel Weight:</b> {{$courier_detail->weight}}</p>  </div>
                <div class="col-3"> <p><b>Courier Use:</b> {{$courier_detail->courier_type}}</p> </div>
              </div>

              <div class="offset-1">
                <h5>Reciever Details</h5>
                <div class="row">
                  <div class="col-3"> <p> <b>Name:</b> {{$courier_detail->address->name_reciever}}</p> </div>
                  <div class="col-3"> <p> <b>Email:</b> {{$courier_detail->address->email_reciever}}</p> </div>
                  <div class="col-3"> <p> <b>Phone No:</b> {{$courier_detail->address->contact_reciever}}</p> </div>
                </div>

                <div class="col-6">
                  <p class="m-0"> <b>Address:</b> {{$courier_detail->address->address_reciever}}</p>
                  <p class="m-0" style="padding-left: 69px;">{{$courier_detail->address->address_reciever1}}</p>
                  <p class="m-0"style="padding-left: 69px;">{{$courier_detail->address->address_reciever2}}</p>
                  <p class="m-0"style="padding-left: 69px;">{{$courier_detail->address->postcode_reciever}}</p>
                  <p class="m-0"style="padding-left: 69px;">{{$courier_detail->address->city_reciever}}</p>
                  <p class="m-0"style="padding-left: 69px;">{{$courier_detail->address->state_reciever}}</p>
                  <p class="m-0"style="padding-left: 69px;">{{$courier_detail->address->country_reciever}}</p>
                </div>

              </div>

              <div class="float-right">
                <a href="{{ url('/service4/'.$courier_detail->id) }}" class="btn btn-primary">Check Out</a>
              </div>

            </div>
          </div>

      </div>

@endsection
