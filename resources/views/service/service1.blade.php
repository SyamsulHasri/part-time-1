@extends('layout.default')

@section('content')

    <div class="container my-5 py-5">
        <div class="row bs-wizard" style="border-bottom:0;">
                <div class="col-lg-3 ">
                  <div class="text-center ">Parcel Details</div>
                  <div class="progress">
                      <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Delivery Details</div>
                  <div class="progress">
                      <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3 ">
                  <div class="text-center">Order Summary</div>
                  <div class="progress">
                      <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>

                <div class="col-lg-3">
                  <div class="text-center">Payment</div>
                  <div class="progress">
                      <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>
                </div>
        </div>
    </div>

      <div class="container">
          <form method="post" action="{{ route('parcel.details')}}" name="test">
            {{ csrf_field() }}
            <div class="form-row">
              <!-- Parcel Content -->
              <div class="col-3 ml-5" style="padding-right: 20px;">
                <label class="" for="inlineFormInputGroup">Parcel Content</label>
                <div class="input-group mb-2">
                  <input type="text" class="form-control" id="inlineFormInputGroup" name="parcel_content" placeholder="Eg. Book">
                   <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-archive"></i></div>
                  </div>
                </div>
              </div>
              <!-- Value -->
              <div class="col-3" style="padding-right: 20px;">
                <label class="" for="inlineFormInputGroup">Value(RM)</label>
                <div class="input-group mb-2">
                  <input type="text" class="form-control" id="inlineFormInputGroup" name="value" placeholder="0.00">
                   <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                  </div>
                </div>
              </div>
              <!-- Weight -->
              <div class="col-3 mr-3">
                <label class="" for="inlineFormInputGroup">Weight(KG)</label>
                <div class="input-group mb-2">
                  <input type="text" class="form-control" name="weight" placeholder="0.00" onKeyUp="fillOtherFiels()">
                   <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-weight"></i></div>
                  </div>
                </div>
              </div>



              <div class="col-2" style="margin-top: 32px;margin-left: 25px;">
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
              </div>
            </div>

                <div class="row ml-4 mt-3 mb-5 col-12">
                        <div class="form-group row col-3">
                          <label>Courier Name</label>
                          <div class="input-group mb-2">
                            <input type="text" class="form-control" name="courier_type" value="Courier Example" readonly>
                             <div class="input-group-prepend">
                              <div class="input-group-text"><i class="fas fa-shipping-fast"></i></div>
                            </div>
                          </div>
                          <!-- <input type="text" class="form-control" name="courier_type" value="Courier Example" readonly> -->
                        </div>

                        <div class="form-group row col-3 offset-1">
                          <label>Deliver in</label>
                          <div class="input-group mb-2">
                            <input type="text" class="form-control" name="delivery_days" value="2 days" readonly>
                             <div class="input-group-prepend">
                              <div class="input-group-text"><i class="fas fa-calendar-check"></i></div>
                            </div>
                          </div>
                          <!-- <input type="text" class="form-control" name="delivery_days" value="2 days" readonly> -->
                        </div>

                        <div class="form-group row col-3 offset-1">
                          <label>Your Rate</label>
                          <div class="input-group mb-2">
                            <input type="text" class="form-control" name="price" readonly="true">
                             <div class="input-group-prepend">
                              <div class="input-group-text"><i class="fas fa-money-bill-wave"></i></div>
                            </div>
                          </div>
                          <!-- <input type="text" class="form-control" name="price" readonly="true"> -->
                        </div>
                </div>

               <!--  <div class="row ml-5 mt-5 col-12">
                        <div class="form-group row col-4">
                          <label>Courier Name</label>
                          <input type="text" class="form-control" name="courier" value="Courier Example" disabled>
                        </div>

                        <div class="form-group row col-4">
                          <label>Deliver in</label>
                          <input type="text" class="form-control" name="delivery_days" value="2 days" disabled>
                        </div>

                        <div class="form-group row col-4">
                          <label>Your Rate</label>
                          <input type="text" class="form-control" name="your_rate1" readonly="true">
                        </div>
                </div> -->

          </form>
      </div>



@endsection


<script type="text/javascript">
function fillOtherFiels()
{
   var fieldOneValue = document.forms["test"].weight.value;
    if (isNaN(fieldOneValue) == false)
    {
        document.forms["test"].price.value = parseInt(fieldOneValue) * 7.00;
        document.forms["test"].your_rate1.value = parseInt(fieldOneValue) * 8.50;
    } else {
         document.forms["test"].price.value = 0;
         document.forms["test"].your_rate1.value = 0;
    }
}
</script>
