  <!-- CAROUSEL -->
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active container1">
            <img class="d-block w-100" src="img/truck.jpg" alt="First slide" style="height: 100vh;">
            <div class=" d-none d-md-block">
              <div class="row centered">

                <h1 class="roboto">RA</h1>

                <div class="form-group pl-2 pt-2">
                  <h4 class="roboto mb-0">NETWORK</h4>
                  <h4 class="roboto float-left">SERVICE</h4>
                </div>

              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/truck.jpg" alt="Second slide" style="height: 100vh;">
            <div class=" d-none d-md-block">
              <div class="row centered">

                <h1 class="roboto">RA</h1>

                <div class="form-group pl-2 pt-2">
                  <h4 class="roboto mb-0">NETWORK</h4>
                  <h4 class="roboto float-left">SERVICE</h4>
                </div>

              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/truck.jpg" alt="Third slide" style="height: 100vh;">
            <div class=" d-none d-md-block">
              <div class="row centered">

                <h1 class="roboto">RA</h1>

                <div class="form-group pl-2 pt-2">
                  <h4 class="roboto mb-0">NETWORK</h4>
                  <h4 class="roboto float-left">SERVICE</h4>
                </div>

              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      <div id="about">
        <div class="container">
           <h4 class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quidem, iure. Incidunt nihil porro, vel, libero quibusdam mollitia blanditiis, ab corrupti fugiat similique obcaecati quaerat accusantium eum nam aperiam! Ratione.</h4>

           <div class="form-group">

            <div class="row text-center pt-5">
              <div class="col-4">
                <i class="fas fa-people-carry fa-2x"></i>
                <p>Lorem Ipsum</p>
              </div>
              <div class="col-4">
                <i class="fas fa-truck fa-2x"></i>
                <p>Lorem Ipsum</p>
              </div>
              <div class="col-4">
                <i class="fas fa-building fa-2x"></i>
                <p>Lorem Ipsum</p>
              </div>
            </div>

            <div class="row text-center pt-5">
              <div class="col-4">
                <i class="fas fa-boxes fa-2x"></i>
                <p>Lorem Ipsum</p>
              </div>
              <div class="col-4">
                <i class="fas fa-store fa-2x"></i>
                <p>Lorem Ipsum</p>
              </div>
              <div class="col-4">
                <i class="fas fa-money-bill fa-2x"></i>
                <p>Lorem Ipsum</p>
              </div>
            </div>

           </div>


        </div>

      </div>

      <div id="partition" class="container-fluid">

        <div class="row">
          <div class="col-8 text-center pt-5">

            <h3 class="roboto text-danger font-weight-bold ">SERVICE</h3>

             <div class="form-group">

              <div class="row text-center pt-5">
                <div class="col-4">
                  <i class="fas fa-people-carry fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-truck fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-building fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
              </div>

              <div class="row text-center pt-5">
                <div class="col-4">
                  <i class="fas fa-boxes fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-store fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-money-bill fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
              </div>

           </div>

          </div>

          <div class="col-4">
            <img src="img/service.jpg" height="400">

          </div>
        </div>

      </div>

      <div id="partition" class="container-fluid">

        <div class="row">
          <div class="col-4">
            <img src="img/customer.jpg" height="400">
          </div>

          <div class="col-8 text-center pt-5">
            <h3 class="roboto text-danger font-weight-bold">CUSTOMER</h3>

             <div class="form-group">

              <div class="row text-center pt-5">
                <div class="col-4">
                  <i class="fas fa-people-carry fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-truck fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-building fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
              </div>

              <div class="row text-center pt-5">
                <div class="col-4">
                  <i class="fas fa-boxes fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-store fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-money-bill fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
              </div>

           </div>

          </div>
        </div>

      </div>

      <div id="partition" class="container-fluid">

        <div class="row">
          <div class="col-8 text-center pt-5">
            <h3 class="roboto text-danger font-weight-bold">E-COMMERCE</h3>

             <div class="form-group">

              <div class="row text-center pt-5">
                <div class="col-4">
                  <i class="fas fa-people-carry fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-truck fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-building fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
              </div>

              <div class="row text-center pt-5">
                <div class="col-4">
                  <i class="fas fa-boxes fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-store fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
                <div class="col-4">
                  <i class="fas fa-money-bill fa-2x"></i>
                  <p>Lorem Ipsum</p>
                </div>
              </div>

           </div>

          </div>

          <div class="col-4">
            <img src="img/e-commerce.jpg" height="400">

          </div>

      </div>

      </div>

      <div id="partition" class="container-fluid">

        <div class="container1">
          <img src="img/location.png" height="400">
          <div class="top-right">
            <h3 class="roboto text-danger font-weight-bold">LOCATION</h3>
          </div>
          <div class="centered">

            <div class="container">
              <div class="row pt-5" style="padding-left: 10px;">
                <div class="col-4">
                  <a class="btn btnclr btn-lg text-dark" href="#" role="button">LOCATION 1</a>
                </div>
                <div class="col-2"></div>
                <div class="col-4">
                  <a class="btn btnclr btn-lg text-dark" href="#" role="button">LOCATION 2</a>
                </div>
              </div>
            </div>

            <div class="container">
              <div class="row pt-5" style="padding-left: 10px;">
                <div class="col-4">
                  <a class="btn btnclr btn-lg text-dark" href="#" role="button">LOCATION 3</a>
                </div>
                <div class="col-2"></div>
                <div class="col-4">
                  <a class="btn btnclr btn-lg text-dark" href="#" role="button">LOCATION 4</a>
                </div>
              </div>

            </div>

        </div>

      </div>
