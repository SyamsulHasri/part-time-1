      <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <img src="/img/footer_logo.png" alt="Logo" height="55" width="55">
          <div class="box">
            <div class="link">
              <a class="linkTxt" href="#">TERMS &amp; CONDITIONS</a>
              <span class="seperator"></span>
              <a href="/#" class="linkTxt">PRIVACY POLICY</a>
            </div>
            <div class="address">ADDRESS, Example of address, No., City, Poscode, State, Malaysia</div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="copyRight">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span class="copyRightTxt">All Copyrights Reserved &copy; 2018</span>
        </div>
        <div class="col-md-6 alignRight">
          <div class="box">
            <span class="copyRightTxt">Telefon : 603 3333 3333</span>
            <span class="seperator"></span>
            <span class="copyRightTxt">Fax : 603 2222 2222</span>
          </div>
        </div>
      </div>
    </div>
  </div>
