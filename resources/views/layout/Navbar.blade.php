<!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white">
        <a class="navbar-brand text-danger roboto" href="#">
            <img src="/img/logo.jpeg" width="40" height="40">
            <b>RA Network Service</b>
            </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link text-danger roboto " href="#">HOME<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger roboto" href="/service">SERVICES</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger roboto" href="#">CUSTOMER</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger roboto" href="#">E-COMMERCE</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger roboto" href="#">LOCATION</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger roboto" href="#">NOTICE</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
