<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParcelAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcelDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parcels_id');
            $table->foreign('parcels_id')->references('id')->on('courier');

            $table->string('name_sender');
            $table->string('email_sender');
            $table->string('contact_sender');
            $table->string('alt_contact_sender');
            $table->string('company_sender');
            $table->string('address_sender');
            $table->string('address_sender1');
            $table->string('address_sender2');
            $table->string('city_sender');
            $table->string('postcode_sender');
            $table->string('state_sender');
            $table->string('country_sender');

            $table->string('name_reciever');
            $table->string('email_reciever');
            $table->string('contact_reciever');
            $table->string('alt_contact_reciever');
            $table->string('company_reciever');
            $table->string('address_reciever');
            $table->string('address_reciever1');
            $table->string('address_reciever2');
            $table->string('city_reciever');
            $table->string('postcode_reciever');
            $table->string('state_reciever');
            $table->string('country_reciever');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
